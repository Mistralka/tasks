var sampleApp = angular.module("sampleApp", []);
sampleApp.factory('StringConstants', function () {
    var constants = 
        { 
            auth: {
                'user_not_found': 'Пользователь не найден',
                'wrong_password': 'Неверный пароль'
            },
            viewSwitcher: {
                'detailed_view': 'Подробный вид',
                'quick_view': 'Краткий вид',
                'filter_view': 'Вид для фильтрации'
            }
        
        }
    return {
      constants: constants
    }
});

  

 