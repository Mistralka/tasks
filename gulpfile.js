'use strict';

var gulp = require('gulp');
var browserSync = require('browser-sync');
var filesToWatch = ['task-app.js', 'index.html', 'css/**/*.css'];

gulp.task('watch', function() {
  gulp.watch(filesToWatch);
});

gulp.task('serve', ['watch'], function() {
  browserSync.init(filesToWatch, {
    startPath: '/',
    server: {
      baseDir: '.',
      middleware: [proxyMiddleware ([
        '^[^\\.]*$ /index.html [L]'
      ])]
    },
    browser: 'default'
  });
});

gulp.task('default', function() {
  gulp.start('serve');
});

function proxyMiddleware(req, res, next) {
  if (req.url.indexOf('/api/') !== -1) {
    proxy.web(req, res);
  } else {
    next();
  }
}
