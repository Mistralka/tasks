sampleApp.controller("defaultCtrl", function ($scope, $http, $interval, StringConstants) {

        $scope.users = "";
        $http.get('users.json').success(function (data) {
            $scope.users = data;
        });

        // Автообновление списка

        $interval(function() {
            $http.get('users.json').success(function (data) {
                $scope.users = data;
             });
         }, 20000);

        $scope.authPage = true; 
        $scope.tasksPage = false;
        $scope.user = 0;

        $scope.password = "";
        $scope.login = "";


        $scope.findUser = function(users, login) {
            var i, l;
            for(i = 0, l = users.length; i < l; i++) { // поиск имени пользователя в массиве
                if (users[i]["user_name"] === login) {
                    return users[i];
                }                                                     
            }
            return null;
        };

        $scope.validatePass = function(user, password) {
            if (!!user && !!user["user_password"]) {
                return user["user_password"] === password;
            }
            return false;
        };

        $scope.authorize = function(login, password) {
            $scope.exit();
            $scope.user = $scope.findUser($scope.users, login);

            if (!$scope.user) {
                $scope.message = StringConstants.constants.auth.user_not_found;
            } else {
                if ($scope.validatePass($scope.user, password)) {
                    $scope.message =  '';
                    $scope.authPage = false; 
                    $scope.tasksPage= true;
                } else {
                    $scope.message = StringConstants.constants.auth.wrong_password;
                }
            }

            return $scope.user;
        };

        // Выход из сессии

        $scope.exit = function() {
            $scope.authPage = true; 
            $scope.tasksPage = false;
            $scope.message = "";
        };

        // Переключение между списком задач и страницей задачи

        $scope.tasksListPage = true;
        $scope.tasksItemPage = false;

        $scope.showTasksItemPage = function(index) {
            $scope.tasksItemPage = true;
            $scope.tasksListPage = false;
            $scope.index = index; 
        }
        $scope.showTasksListPage = function() {
            $scope.tasksListPage = true;
            $scope.tasksItemPage = false;
        }

        // Смена представления таблицы

        $scope.selectedClass = "table_big";
        $scope.classNames =
            [{ displayName: StringConstants.constants.viewSwitcher.detailed_view, value: "table_big" },
            { displayName: StringConstants.constants.viewSwitcher.quick_view, value: "table_small" },
            { displayName: StringConstants.constants.viewSwitcher.filter_view, value: "table_sort" }];

    });